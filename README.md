

# But 

Ce module permet de générer un fichier `.svg` à partir d'un calendrier fourni en entrée.

# Installation

Le module est contenu dans un fichier `calendarPrinter.py`, qu'il faut  mettre
dans le même dossier que le fichier contenant le code python que l'on souhaite exécuter.
On l'importe avec la ligne suivante `from calendarPrinter import *`.

Ce module utilise également le module `svgwriter` :
- doc : https://svgwrite.readthedocs.io/en/latest/
- module : https://pypi.org/project/svgwrite/

Pour l’installation, cela dépend de votre OS, sur Linux `pip install svgwriter` suffit (sur macOS peut-être aussi, noyau UNIX).

Le module utilise aussi les modules `json` et `datetime` mais ils sont normalement déjà installés avec le python de base. 
- module `json`
  - doc : https://docs.python.org/fr/3/library/json.html?highlight=json#module-json
- module `datetime`
  - doc : https://docs.python.org/fr/3/library/datetime.html

Je vous conseille également `inkscape` afin de modifier facilement le `.svg` généré.

# Format du calendrier

Les fonctions du module utilisent un 'objet' calendrier, qui n'est autre qu'une liste de fenêtres temporelles.

Une fenêtre temporelle est une période entre 2 datetimes, avec le format suivant :
```[start, end, couleur, label]```
- start : début de la fenêtre temporelle, du type datetime (voir module correspondant).
- end : fin de la fenêtre temporelle, du type datetime (voir module correspondant).
- couleur : couleur de la fenêtre temporelle, du type str (par exemple "#00FF00").
- label : texte de la fenêtre temporelle, du type str.

Attention, on suppose ici que `start < end`.

Un calendrier est donc une liste de fenêtres temporelles.

Par exemple :
```
[[datetime.datetime(2021, 10, 20, 8, 0), datetime.datetime(2021, 10, 20, 10, 0), '#FF0000', 'job0'],
 [datetime.datetime(2021, 10, 20, 10, 0), datetime.datetime(2021, 10, 20, 12, 0), '#FF0000', 'job1'],
 [datetime.datetime(2021, 10, 20, 13, 0), datetime.datetime(2021, 10, 20, 15, 0), '#FF0000', 'job4'],
 [datetime.datetime(2021, 10, 21, 8, 0), datetime.datetime(2021, 10, 21, 10, 0), '#FF0000', 'job8'],
 [datetime.datetime(2021, 10, 21, 10, 0), datetime.datetime(2021, 10, 21, 12, 0), '#FF0000', 'job9'],
 [datetime.datetime(2021, 10, 21, 13, 0), datetime.datetime(2021, 10, 21, 15, 0), '#FF0000', 'job10']]
```

# Fonctions du module

Le module comporte 2 fonctions :

```borneDuree(calendar)```

cette fonction permet d'avoir les bornes temporelles du calendrier.
- entrées
  - un calendrier (une liste de fenêtres temporelles)
- sorties
  - startCal : date, date la plus ancienne parmi les fenêtres temporelles
  - endCal : date, date la plus récente parmi les fenêtres temporelles
  - startDay : time, heure la plus 'tôt' parmi les fenêtres temporelles
  - endDay : time, heure la plus 'tard' parmi les fenêtres temporelles

  
```printCalendar(name,file,cal,startCal,endCal,startDay,endDay)```
- entrées
  - name : str, titre du calendrier
  - file : str, nom du fichier dans lequel on écrira l'image
  - cal : calendrier, liste des fenêtres temporelles que l'on souhaite afficher
  - startCal : date, date du premier jour
  - endCal : date, date du dernier jour (bornes incluses)
  - startDay : time, heure du début de journée
  - endDay : time, heure de fin de journée (bornes incluses)
- sorties
  - aucune, écrit simplement le fichier avec les paramètres fournis.

# Usage
Après avoir lu le fichier `.json` contenant votre modèle, il faut construire un ou plusieurs calendrier(s). (1 global avec un jeu de couleurs, ou 1 par employé)

## Mise en garde
- Ce module ne prend pas en compte les fenêtres temporelles à cheval sur 2 jours ou plus.
- Si dans un même calendrier, 2 fenêtres temporelles s'intersectent, il n'y aura pas d'erreur, mais à l'affichage il y aura juste une superposition des rectangles.


# Exemple

Ce dépôt comporte 2 exemples :
- Lecture des calendriers d'une instance `instance1.json` par le programme `calendarWrsp.py`, et création des images dans le dossier `outWrsp`
- Lecture des calendriers d'une solution `sol.json` par le programme `calendarSol.py`, et création des images dans le dossier `outSol`

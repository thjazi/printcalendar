from datetime import *
from json import loads
from calendarPrinter import printCalendar, borneDuree


nameOfFile = "instance1.json"


def color_from_cost(c):
	val = int(200 * (1-c) - 50)
	if val >= 255 : val = 255
	if val <= 0 : val = 0
	if val == 0:
		return "#4A4A4A"
	else:
		val = hex(val)
		val = str(val)[2:]
		return "#FF{:0>2}{:0>2}".format(val,val)



def extractCalendar(data):
	# recupere les differents calendrier
	# avec la fonction get Calendar
	CALS = []
	for idEmp in data['employees']:
		emplo = data['employees'][idEmp]
		dispo = emplo['vacancies']
		
		cal = ["emp"+idEmp,[]]
		for zone in dispo:
			startDatetime = datetime.utcfromtimestamp(zone['startTime'])
			endDatetime = datetime.utcfromtimestamp(zone['endTime'])
			cal[1].append([startDatetime,endDatetime,color_from_cost(zone["cost"]),str(zone["cost"])])
		CALS.append(cal)

	for idJob in data['jobs']:
		jo = data['jobs'][idJob]
		dispo = jo["calendar"]

		cal = ["job"+idJob,[]]
		for zone in dispo:
			startDatetime = datetime.utcfromtimestamp(zone['startTime'])
			endDatetime = datetime.utcfromtimestamp(zone['endTime'])
			cal[1].append([startDatetime,endDatetime,color_from_cost(zone["cost"]),str(zone["cost"])])
		CALS.append(cal)


	return CALS


file = open(nameOfFile,'r')
data = loads(file.read())

calendars = extractCalendar(data)

allPeriode = []
for c in calendars:
	allPeriode += c[1]

startCal,endCal,startDay,endDay = borneDuree(allPeriode)

for calendar in calendars:
	printCalendar(calendar[0],"outWrsp/"+calendar[0]+".svg",calendar[1],startCal,endCal,startDay,endDay)


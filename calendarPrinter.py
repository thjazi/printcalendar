import svgwrite
from datetime import *

def printCalendar(name,file,cal,startCal,endCal,startDay,endDay):

	marge = 50 #pxl
	marge_haut = marge + 10 # pour le titre

	largeur_image = 1200
	hauteur_image = 900

	color_line = "black"
	width_line = 3
	step = timedelta(minutes=30)


	## calculs des données importantes
	largeur_canvas = largeur_image - marge*2
	hauteur_canvas = hauteur_image - marge - marge_haut

	number_day = endCal - startCal + timedelta(days=1)
	number_day = number_day.days

	largeur_jour = largeur_canvas//number_day
	hauteur_jour = hauteur_canvas
	
	duree_jour = datetime(2020,2,3,endDay.hour,endDay.minute,endDay.second) - datetime(2020,2,3,startDay.hour,startDay.minute,startDay.second)
	duree_jour = int(duree_jour.total_seconds())


	## ecriture du svg

	svg_document = svgwrite.Drawing(filename = file, size = ("{}px".format(largeur_image), "{}px".format(hauteur_image)))

	svg_document.add(svg_document.text(name,style="text-anchor:middle", insert = (largeur_image//2, marge_haut//2)))
	


	## affichage des lignes horizontales + etiquettes
	label = datetime(2020,2,3,startDay.hour,startDay.minute,startDay.second)
	for i in range(0,duree_jour+int(step.total_seconds()),int(step.total_seconds())):

		X_text = marge - 5
		Y_text = int(marge_haut + hauteur_jour * i / duree_jour)
		tex = label.strftime("%Hh%M")
		label += step

		svg_document.add(svg_document.text(tex,style="text-anchor:end",insert=(X_text,Y_text)))
		svg_document.add(svg_document.line(start=(marge,Y_text),
			end=(marge+largeur_canvas,Y_text),
		 	stroke=color_line, stroke_width=width_line))


	## affichage des lignes verticales + etiquettes
	for i in range(0,number_day,1):

		Xa_text = (marge + i * largeur_jour)
		Xb_text = (marge + (i+1) * largeur_jour)
		Y_text = marge_haut - 10
		label = startCal + timedelta(days=i)
		tex = label.strftime("%d/%m/%Y")
		svg_document.add(svg_document.text(tex,style="text-anchor:middle",insert=((Xa_text+Xb_text)//2,Y_text)))
		svg_document.add(svg_document.line(start=(Xa_text,marge_haut),
			end=(Xa_text,marge_haut+hauteur_canvas),
		 	stroke=color_line, stroke_width=width_line))


	svg_document.add(svg_document.line(start=(Xb_text,marge_haut),
		end=(Xb_text,marge_haut+hauteur_canvas),
	 	stroke=color_line, stroke_width=width_line))


	## ecriture des zones temporelles
	for zone in cal:
		startZone = date(zone[0].year,zone[0].month,zone[0].day)
		endZone = date(zone[1].year,zone[1].month,zone[1].day)

		if startZone != endZone:
			raise ValueError("L'affichage ne prend pas en charge les zones a cheval sur 2 jours")
	
		n_day = startZone - startCal
		n_day = n_day.days

		duree_zone = zone[1] - zone[0]
		duree_zone = duree_zone.total_seconds()

		debut_zone = zone[0] - datetime(startZone.year,startZone.month,startZone.day,startDay.hour,startDay.minute,startDay.second)
		debut_zone = debut_zone.total_seconds()


		X_zone = marge + n_day * largeur_jour
		X_size = largeur_jour
		Y_zone = int(marge_haut + hauteur_jour * debut_zone / duree_jour)
		Y_size = int(hauteur_jour * duree_zone / duree_jour)

		svg_document.add(svg_document.rect(insert = (X_zone, Y_zone),
                                   size = ("{}px".format(X_size), "{}px".format(Y_size)),
                                   stroke_width = "1",
                                   stroke = "black",
                                   fill = zone[2]))

		X_text = ((X_zone+X_size) + X_zone)//2
		Y_text = ((Y_zone+Y_size) + Y_zone)//2
		svg_document.add(svg_document.text(zone[3],style="text-anchor:middle", insert = (X_text, Y_text)))		
	
	svg_document.save()



def borneDuree(calendar):
	dateList = []
	timeList = []

	for zone in calendar:
		startDatetime = zone[0]
		endDatetime = zone[1]

		sday = date(startDatetime.year,startDatetime.month,startDatetime.day)
		eday = date(endDatetime.year,endDatetime.month,endDatetime.day)

		stime = time(startDatetime.hour,startDatetime.minute,startDatetime.second)
		etime = time(endDatetime.hour,endDatetime.minute,endDatetime.second)

		dateList.append(sday)
		dateList.append(eday)
		timeList.append(stime)
		timeList.append(etime)
	
	return min(dateList),max(dateList),min(timeList),max(timeList)


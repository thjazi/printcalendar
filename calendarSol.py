from datetime import *
from json import loads
from calendarPrinter import printCalendar, borneDuree


nameOfFile = "sol.json"


def extractCalendar(data):
	# recupere les differents calendrier
	# avec la fonction get Calendar
	CALS = []
	for employe in data['emp_affect']:
		aff = data['emp_affect'][employe]
		
		a = employe.index("id=")
		b = employe.index(", ")
		
		emp = "emp"+employe[a+3:b]
		cal = [emp,[]]
		for affectation in aff:
			plage = affectation['window']
			startDatetime = datetime.utcfromtimestamp(plage['startTime'])
			endDatetime = datetime.utcfromtimestamp(plage['endTime'])
			job = affectation['job']["id"]			
			cal[1].append([startDatetime,endDatetime,"#FF0000","job"+str(job)])
		CALS.append(cal)


	for job in data["job_affect"]:
		aff = data["job_affect"][job]['window']

		a = job.index("id=")
		b = job.index(", ")
		j = "job"+ job[a+3:b]
			
		startDatetime = datetime.utcfromtimestamp(aff['startTime'])
		endDatetime = datetime.utcfromtimestamp(aff['endTime'])
		
		cal = [j,[[startDatetime,endDatetime,"#FF0000",j]]]

		CALS.append(cal)

	return CALS


file = open(nameOfFile,'r')
data = loads(file.read())

calendars = extractCalendar(data)

allPeriode = []
for c in calendars:
	allPeriode += c[1]

startCal,endCal,startDay,endDay = borneDuree(allPeriode)

for calendar in calendars:
	printCalendar(calendar[0],"outSol/"+calendar[0]+".svg",calendar[1],startCal,endCal,startDay,endDay)

